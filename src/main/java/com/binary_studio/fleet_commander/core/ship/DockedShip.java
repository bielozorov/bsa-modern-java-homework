package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

    private final String name;

    private final PositiveInteger shieldHP;

    private final PositiveInteger hullHP;

    private final PositiveInteger powergridOutput;

    private final PositiveInteger capacitorAmount;

    private final PositiveInteger capacitorRechargeRate;

    private final PositiveInteger speed;

    private final PositiveInteger size;

    private AttackSubsystem attackSubsystem;

    private DefenciveSubsystem defenciveSubsystem;

    public static DockedShip construct(String name,
                                       PositiveInteger shieldHP,
                                       PositiveInteger hullHP,
                                       PositiveInteger powergridOutput,
                                       PositiveInteger capacitorAmount,
                                       PositiveInteger capacitorRechargeRate,
                                       PositiveInteger speed,
                                       PositiveInteger size) {
        return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
                size);
    }

    private DockedShip(String name,
                       PositiveInteger shieldHP,
                       PositiveInteger hullHP,
                       PositiveInteger powergridOutput,
                       PositiveInteger capacitorAmount,
                       PositiveInteger capacitorRechargeRate,
                       PositiveInteger speed,
                       PositiveInteger size) {
        this.name = name;
        this.shieldHP = shieldHP;
        this.hullHP = hullHP;
        this.powergridOutput = powergridOutput;
        this.capacitorAmount = capacitorAmount;
        this.capacitorRechargeRate = capacitorRechargeRate;
        this.speed = speed;
        this.size = size;
    }

    @Override
    public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
        PositiveInteger consumption = getConsumption(subsystem, defenciveSubsystem);
        if (checkConsumption(consumption)) {
            attackSubsystem = subsystem;
        } else {
            throw new InsufficientPowergridException(consumption.value() - powergridOutput.value());
        }
    }

    @Override
    public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
        PositiveInteger consumption = getConsumption(attackSubsystem, subsystem);
        if (checkConsumption(consumption)) {
            defenciveSubsystem = subsystem;
        } else {
            throw new InsufficientPowergridException(consumption.value() - powergridOutput.value());
        }
    }

    public CombatReadyShip undock() throws NotAllSubsystemsFitted {
        if (attackSubsystem == null && defenciveSubsystem == null) {
            throw NotAllSubsystemsFitted.bothMissing();
        } else if (attackSubsystem == null) {
            throw NotAllSubsystemsFitted.attackMissing();
        } else if (defenciveSubsystem == null) {
            throw NotAllSubsystemsFitted.defenciveMissing();
        } else {
            return CombatReadyShip.construct(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed, size, attackSubsystem, defenciveSubsystem);
        }
    }

    private PositiveInteger getConsumption(AttackSubsystem attackSubsystem, DefenciveSubsystem defenciveSubsystem) {
        int attack = attackSubsystem == null ? 0 : attackSubsystem.getPowerGridConsumption().value();
        int defencive = defenciveSubsystem == null ? 0 : defenciveSubsystem.getPowerGridConsumption().value();
        return PositiveInteger.of(attack + defencive);
    }

    private boolean checkConsumption(PositiveInteger consumption) {
        return consumption.value() <= powergridOutput.value();
    }

}