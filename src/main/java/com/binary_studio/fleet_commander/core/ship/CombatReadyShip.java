package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

    private final String name;

    private final PositiveInteger shieldHP;

    private final PositiveInteger hullHP;

    private final PositiveInteger capacitorAmount;

    private final PositiveInteger capacitorRechargeRate;

    private final PositiveInteger speed;

    private final PositiveInteger size;

    private final AttackSubsystem attackSubsystem;

    private final DefenciveSubsystem defenciveSubsystem;

    private PositiveInteger changeShieldHP;

    private PositiveInteger changeHullHP;

    private PositiveInteger changeCapacitorAmount;


    public static CombatReadyShip construct(String name,
                                            PositiveInteger shieldHP,
                                            PositiveInteger hullHP,
                                            PositiveInteger powergridOutput,
                                            PositiveInteger capacitorAmount,
                                            PositiveInteger capacitorRechargeRate,
                                            PositiveInteger speed,
                                            PositiveInteger size,
                                            AttackSubsystem attackSubsystem,
                                            DefenciveSubsystem defenciveSubsystem) {
        return new CombatReadyShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed, size, attackSubsystem, defenciveSubsystem);
    }

    private CombatReadyShip(String name,
                            PositiveInteger shieldHP,
                            PositiveInteger hullHP,
                            PositiveInteger powergridOutput,
                            PositiveInteger capacitorAmount,
                            PositiveInteger capacitorRechargeRate,
                            PositiveInteger speed,
                            PositiveInteger size,
                            AttackSubsystem attackSubsystem,
                            DefenciveSubsystem defenciveSubsystem) {
        this.name = name;
        this.shieldHP = shieldHP;
        this.hullHP = hullHP;
        this.capacitorAmount = capacitorAmount;
        this.capacitorRechargeRate = capacitorRechargeRate;
        this.speed = speed;
        this.size = size;
        this.attackSubsystem = attackSubsystem;
        this.defenciveSubsystem = defenciveSubsystem;
        this.changeShieldHP = shieldHP;
        this.changeHullHP = hullHP;
        this.changeCapacitorAmount = capacitorAmount;
    }

    @Override
    public void endTurn() {
        changeCapacitorAmount = PositiveInteger.of(Math.min(capacitorRechargeRate.value() + changeCapacitorAmount.value(), capacitorAmount.value()));
    }

    @Override
    public void startTurn() {
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public PositiveInteger getSize() {
        return size;
    }

    @Override
    public PositiveInteger getCurrentSpeed() {
        return speed;
    }

    @Override
    public Optional<AttackAction> attack(Attackable target) {
        if (attackSubsystem.getCapacitorConsumption().value() <= changeCapacitorAmount.value()) {
            changeCapacitorAmount = PositiveInteger.of(changeCapacitorAmount.value() - attackSubsystem.getCapacitorConsumption().value());
            return Optional.of(new AttackAction(attackSubsystem.attack(target), this, target, attackSubsystem));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public AttackResult applyAttack(AttackAction attack) {
        if (changeShieldHP.value() - defenciveSubsystem.reduceDamage(attack).damage.value() >= 0) {
            changeShieldHP = PositiveInteger.of(changeShieldHP.value() - defenciveSubsystem.reduceDamage(attack).damage.value());
            return new AttackResult.DamageRecived(defenciveSubsystem.reduceDamage(attack).weapon, defenciveSubsystem.reduceDamage(attack).damage, this);
        }
        changeShieldHP = PositiveInteger.of(0);
        if (changeHullHP.value() - PositiveInteger.of(Math.abs(changeShieldHP.value() - defenciveSubsystem.reduceDamage(attack).damage.value())).value() > 0) {
            changeHullHP = PositiveInteger.of(changeHullHP.value() - PositiveInteger.of(Math.abs(changeShieldHP.value() - defenciveSubsystem.reduceDamage(attack).damage.value())).value());
            return new AttackResult.DamageRecived(defenciveSubsystem.reduceDamage(attack).weapon, defenciveSubsystem.reduceDamage(attack).damage, this);
        }
        return new AttackResult.Destroyed();
    }

    @Override
    public Optional<RegenerateAction> regenerate() {
        if (defenciveSubsystem.getCapacitorConsumption().value() <= changeCapacitorAmount.value()) {
            changeCapacitorAmount = PositiveInteger.of(changeCapacitorAmount.value() - defenciveSubsystem.getCapacitorConsumption().value());
            RegenerateAction hullRegeneratedAction = regenerateHull(defenciveSubsystem.regenerate());
            RegenerateAction shieldRegeneratedAction = regenerateShield(hullRegeneratedAction);
            return Optional.of(shieldRegeneratedAction);
        } else {
            return Optional.empty();
        }
    }

    private RegenerateAction regenerateHull(RegenerateAction regenerateAction) {
        int regeneratedHullHP = 0;
        if (hullHP.value() - changeHullHP.value() != 0) {
            regeneratedHullHP = (hullHP.value() - changeHullHP.value() > regenerateAction.hullHPRegenerated.value()) ? regenerateAction.hullHPRegenerated.value() : hullHP.value() - changeHullHP.value();
            changeHullHP = PositiveInteger.of(regeneratedHullHP);
        }
        return new RegenerateAction(regenerateAction.shieldHPRegenerated, PositiveInteger.of(regeneratedHullHP));
    }

    private RegenerateAction regenerateShield(RegenerateAction regenerateAction) {
        int regeneratedShieldHP = 0;
        if (shieldHP.value() - changeShieldHP.value() != 0) {
            regeneratedShieldHP = (shieldHP.value() - changeShieldHP.value() > regenerateAction.shieldHPRegenerated.value()) ? regenerateAction.shieldHPRegenerated.value() : shieldHP.value() - changeShieldHP.value();
            changeShieldHP = PositiveInteger.of(regeneratedShieldHP);
        }
        return new RegenerateAction(PositiveInteger.of(regeneratedShieldHP), regenerateAction.hullHPRegenerated);
    }

}