package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

    private final String name;

    private final PositiveInteger powergridConsumption;

    private final PositiveInteger capacitorConsumption;

    private final PositiveInteger impactReductionPercent;

    private final PositiveInteger shieldRegeneration;

    private final PositiveInteger hullRegeneration;

    public static DefenciveSubsystemImpl construct(String name,
                                                   PositiveInteger powergridConsumption,
                                                   PositiveInteger capacitorConsumption,
                                                   PositiveInteger impactReductionPercent,
                                                   PositiveInteger shieldRegeneration,
                                                   PositiveInteger hullRegeneration) throws IllegalArgumentException {
        if (name == null || name.isBlank()) {
            throw new IllegalArgumentException("Name should be not null and not empty");
        } else {
            return new DefenciveSubsystemImpl(name, powergridConsumption, capacitorConsumption, impactReductionPercent, shieldRegeneration, hullRegeneration);
        }
    }

    private DefenciveSubsystemImpl(String name,
                                   PositiveInteger powergridConsumption,
                                   PositiveInteger capacitorConsumption,
                                   PositiveInteger impactReductionPercent,
                                   PositiveInteger shieldRegeneration,
                                   PositiveInteger hullRegeneration) {
        this.name = name;
        this.powergridConsumption = powergridConsumption;
        this.capacitorConsumption = capacitorConsumption;
        this.impactReductionPercent = impactReductionPercent.value() < 95 ? impactReductionPercent : PositiveInteger.of(95);
        this.shieldRegeneration = shieldRegeneration;
        this.hullRegeneration = hullRegeneration;
    }

    @Override
    public PositiveInteger getPowerGridConsumption() {
        return powergridConsumption;
    }

    @Override
    public PositiveInteger getCapacitorConsumption() {
        return capacitorConsumption;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public AttackAction reduceDamage(AttackAction incomingDamage) {
        double damage = incomingDamage.damage.value() * (1.0 - impactReductionPercent.value() / 100.0);
        return new AttackAction(PositiveInteger.of((int) Math.ceil(damage)), incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
    }

    @Override
    public RegenerateAction regenerate() {
        return new RegenerateAction(shieldRegeneration, hullRegeneration);
    }

}