package com.binary_studio.academy_coin;

import java.util.stream.Stream;

public final class AcademyCoin {

    private AcademyCoin() {
    }

    public static int maxProfit(Stream<Integer> prices) {
        int[] array = prices.mapToInt(x -> x).toArray();
        int result = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i] > array[i - 1])
                result += array[i] - array[i - 1];
        }
        return result;
    }

}
