package com.binary_studio.dependency_detector;

import java.util.*;

public class ConvertingIntoMathematicalDescription {
    private Map<String, Integer> vertexMap;
    private List<Integer> vertex;
    private List<Integer[]> edge;

    public ConvertingIntoMathematicalDescription(DependencyList libraries) {
        initial(libraries);
    }

    private void initial(DependencyList libraries) {
        if (libraries != null && libraries.libraries != null && libraries.dependencies != null) {
            vertexMap = new LinkedHashMap<>();
            int index = 0;
            for (String library : libraries.libraries) {
                vertexMap.put(library, index++);
            }
        } else {
            System.out.println("Error: libraries or its elements equal null!");
            return;
        }
        vertex = new ArrayList<>();
        for (String key : vertexMap.keySet()) {
            vertex.add(vertexMap.get(key));
        }
        edge = new ArrayList<>();
        for (String[] dependence : libraries.dependencies) {
            Integer[] array = new Integer[dependence.length];
            for (int i = 0; i < array.length; i++) {
                array[i] = vertexMap.get(dependence[i]);
            }
            edge.add(array);
        }
    }

    public Map<String, Integer> getVertexMap() {
        return vertexMap;
    }

    public List<Integer> getVertex() {
        return vertex;
    }

    public List<Integer[]> getEdge() {
        return edge;
    }
}
