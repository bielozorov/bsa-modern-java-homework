package com.binary_studio.dependency_detector;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {
		MathematicalAlgorithm algorithm = new MathematicalAlgorithm(libraries);
		return algorithm.solution();
	}

}
