package com.binary_studio.dependency_detector;

import java.util.ArrayList;

public class MathematicalAlgorithm {

    private final static int WHITE = 0;
    private final static int GREY = 1;
    private final static int BLACK = 2;

    private final ConvertingIntoMathematicalDescription converter;
    private ArrayList<Integer>[] adjacencyList;
    private int[] used;
    private boolean condition;

    public MathematicalAlgorithm(DependencyList libraries) {
        converter = new ConvertingIntoMathematicalDescription(libraries);
        convertingInitialData();
    }

    public boolean solution() {
        used = new int[converter.getVertex().size() + 1];
        for (int i = 1; i <= converter.getVertex().size(); i++) {
            if (used[i] == WHITE) {
                depthFirstSearch(i);
                if (condition) break;
            }
        }
        if (condition) {
            System.out.println("YES Cyclical dependency");
            return false;
        } else {
            System.out.println("NO Cyclical dependency");
            return true;
        }
    }

    private void convertingInitialData() {
        adjacencyList = new ArrayList[converter.getVertex().size() + 1];
        for (int i = 0; i <= converter.getVertex().size(); i++) {
            adjacencyList[i] = new ArrayList<>();
        }
        for (int i = 0; i < converter.getEdge().size(); i++) {
            int a = converter.getEdge().get(i)[0];
            int b = converter.getEdge().get(i)[1];
            adjacencyList[a].add(b);
        }
    }

    private void depthFirstSearch(int vertex) {
        if (condition) {
            return;
        }
        used[vertex] = GREY;
        for (int i = 0; i < adjacencyList[vertex].size(); i++) {
            int v = adjacencyList[vertex].get(i);
            if (used[v] == GREY) {
                condition = true;
                return;
            } else {
                depthFirstSearch(v);
            }
            if (condition) {
                return;
            }
        }
        used[vertex] = BLACK;
    }

}
